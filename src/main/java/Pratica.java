
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import utfpr.ct.dainf.pratica.Lancamento;
import utfpr.ct.dainf.pratica.ProcessaLancamentos;
public class Pratica {
 
    public static void main(String[] args) throws FileNotFoundException, IOException {
        try{
            List<Lancamento> a = new ProcessaLancamentos("src\\main\\resources\\lancamentos.txt").getLancamentos();
            Scanner b = new Scanner(System.in);
            int c = 1;
            System.out.println("Digite a conta a ser buscada: ");
            while (c!=0){
                if (b.hasNextInt())
                    c = b.nextInt();
                else
                    System.out.println("Por favor, informe um valor numérico");
                if (c!=0) exibeLancamentosConta(a, c);
            }
        }catch(Exception aa){
            System.out.println(aa.getLocalizedMessage());
        }
        

    }
    
    public static void exibeLancamentosConta(List<Lancamento> lancamentos, Integer conta) {
        boolean ver = false;
        for(Lancamento b: lancamentos)
            if(b.getConta().equals(conta)){
                System.out.println(b);
                ver = true;
            }
        if (!ver)
            System.out.println("Conta inexistente");
    }
 
}