package utfpr.ct.dainf.pratica;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Linguagem Java
 * @author
 */
public class ProcessaLancamentos {
    private BufferedReader reader;

    public ProcessaLancamentos(File arquivo) throws FileNotFoundException {
        throw new UnsupportedOperationException("Não implementado");
    }

    public ProcessaLancamentos(String path) throws FileNotFoundException {
        reader = new BufferedReader(new FileReader(path));
    }
    
    private String getNextLine() throws IOException {
        if (reader!= null)
            return reader.readLine();
        else
            return null;
    }
    
    private Lancamento processaLinha(String linha) {
        Lancamento a = new Lancamento(Integer.parseInt(linha.substring(0,6)), 
                                        new Date(Integer.parseInt(linha.substring(7,10)), 
                                            Integer.parseInt(linha.substring(10,11)), 
                                            Integer.parseInt(linha.substring(12,13))), 
                                                linha.substring(14,73), 
                                                    Double.parseDouble(linha.substring(74))/100);
        return a;
    }

    
    private Lancamento getNextLancamento() throws IOException {
        String a = getNextLine();
        if(a!=null) return processaLinha(a);
        else return null;
                
    }
    
    public List<Lancamento> getLancamentos() throws IOException {
        List<Lancamento> a = new ArrayList<>();
        Lancamento b = getNextLancamento();
        while(b!=null){
            a.add(b);
            b = getNextLancamento();
        }
        a.sort(new LancamentoComparator());
        return a;
    }
    
}
