package utfpr.ct.dainf.pratica;

import java.util.Comparator;

/**
 * Linguagem Java
 * @author
 */
public class LancamentoComparator implements Comparator<Lancamento>{

    @Override
    public int compare(Lancamento o1, Lancamento o2) {
        int x = o1.getConta();
        int y = o2.getConta();
        if(x<y) return -1;
        else if(x==y){
            return (o1.getData().compareTo(o2.getData()));
        }
        else
            return 1;
    }
    
}
